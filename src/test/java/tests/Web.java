package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class Web {

    public static WebDriver conectar() {

        System.setProperty("webdriver.chrome.driver", "src/Driver/chromedrivervs85/chromedriver.exe");
        WebDriver navegador = new ChromeDriver();

        navegador.get("https://www.sicredi.com.br/html/ferramenta/simulador-investimento-poupanca/");
        return navegador;
    }
}