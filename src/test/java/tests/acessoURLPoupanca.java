package tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class acessoURLPoupanca {

    private WebDriver navegador;

    @Before
    public void Setup() {
        navegador = Web.conectar();
    }

    @Test
    public void testePositivoMeses() {
        WebDriverWait wait = new WebDriverWait(navegador, 10);
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"formInvestimento\"]/div[1]/input[1]")));
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("tempo")));

        // Clicar Box você

        navegador.findElement(By.xpath("//*[@id=\"formInvestimento\"]/div[1]/input[1]")).click();

        //Inserir dados - 20,00 / tempo 12 meses

        navegador.findElement(By.id("valorAplicar")).sendKeys("20,00");
        navegador.findElement(By.id("valorInvestir")).sendKeys("20,00");
        navegador.findElement(By.id("tempo")).sendKeys("12");
        navegador.findElement(By.xpath("//*[@id=\"formInvestimento\"]/div[5]/ul/li[2]/button")).click();

        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("body > div.conteudoGeral.conteudoGeralCompleto.clearfix > div > div > div.formularioBloco.formularioBlocoResultado > div > div.blocoResultadoSimulacao > a")));
        assertEquals("REFAZER A SIMULAÇÃO", navegador.findElement(By.cssSelector("body > div.conteudoGeral.conteudoGeralCompleto.clearfix > div > div > div.formularioBloco.formularioBlocoResultado > div > div.blocoResultadoSimulacao > a")).getText());

        //Na tela do formulário de simulação clicar em "Refazer a simulação" para retornar a tela inicial
        navegador.findElement(By.xpath("/html/body/div[3]/div/div/div[1]/div/div[2]/a")).click();
    }

    @Test
    public void testePositivoAnos() {

        WebDriverWait wait = new WebDriverWait(navegador, 10);
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"formInvestimento\"]/div[1]/input[1]")));
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("tempo")));


        // Clicar box você

        navegador.findElement(By.xpath("//*[@id=\"formInvestimento\"]/div[1]/input[1]")).click();

        //Inserir dados - 20,00 / tempo 2 anos

        navegador.findElement(By.id("valorAplicar")).sendKeys("20,00");
        navegador.findElement(By.id("valorInvestir")).sendKeys("20,00");
        navegador.findElement(By.id("tempo")).sendKeys("2");
        navegador.findElement(By.className("seta")).click();
        navegador.findElement(By.xpath("//*[@id=\"formInvestimento\"]/div[4]/div[2]/div[2]/ul/li[2]/a")).click();
        navegador.findElement(By.xpath("//*[@id=\"formInvestimento\"]/div[5]/ul/li[2]/button")).click();

        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("body > div.conteudoGeral.conteudoGeralCompleto.clearfix > div > div > div.formularioBloco.formularioBlocoResultado > div > div.blocoResultadoSimulacao > a")));
        assertEquals("REFAZER A SIMULAÇÃO", navegador.findElement(By.cssSelector("body > div.conteudoGeral.conteudoGeralCompleto.clearfix > div > div > div.formularioBloco.formularioBlocoResultado > div > div.blocoResultadoSimulacao > a")).getText());

        //Na tela do formulário de simulação clicar em "Refazer a simulação" para retornar a tela inicial

        navegador.findElement(By.xpath("/html/body/div[3]/div/div/div[1]/div/div[2]/a")).click();
    }

    @Test
    public void testeNegativo() {

        WebDriverWait wait = new WebDriverWait(navegador, 10);
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"formInvestimento\"]/div[1]/input[1]")));

        // verificar box voce

        navegador.findElement(By.xpath("//*[@id=\"formInvestimento\"]/div[1]/input[1]")).click();

        //inserir dados - 10,00

        navegador.findElement(By.id("valorAplicar")).sendKeys("10,00");
        navegador.findElement(By.id("valorInvestir")).click();

        //Validar mensagem de erro
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("valorAplicar-error")));
        assertEquals("Valor mínimo de 20.00", navegador.findElement(By.id("valorAplicar-error")).getText());

    }

    @After
    public void tearDown(){

        navegador.quit();

    }
}
